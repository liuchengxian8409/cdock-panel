﻿using DevExpress.XtraBars.Docking;
using SuperMap.Desktop.UI;
using System;
using System.Windows.Forms;

namespace SuperMap.Desktop.Ext
{
    public partial class CDockPanel : UIUserControl
    {
        public CDockPanel()
        {
            InitializeComponent();
            Application.ActiveApplication.MainForm.UILoaded += MainForm_UILoaded;
        }

        private void MainForm_UILoaded(object sender, EventArgs e)
        {
            var dockBarManager = Application.ActiveApplication.MainForm.DockBarManager;
            var dockBar = dockBarManager[typeof(CDockPanel)];
            if (dockBar != null && (dockBar as Control) != null)
            {
                Control control = dockBar as Control;
                DockPanel dockPanel = control.Parent as DockPanel;
                if (dockPanel != null)
                {
                    dockPanel.TabsPosition = TabsPosition.Top;
                    Application.ActiveApplication.MainForm.UILoaded -= MainForm_UILoaded;
                }
            }
        }
    }
}
