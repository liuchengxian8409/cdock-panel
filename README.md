# CDockPanel

#### 介绍
基于 SuperMap iDesktop 桌面端扩展开发自定义浮动面板，可调整浮动面板标签位置。
目前 SuperMap iDesktop 桌面端提供了非常强大的扩展插件功能，用户可以轻易的通过插件配置文件 （*.config） 进行自定义扩展插件在 SuperMap iDesktop 桌面端上布局、状态等调整。此教程主要说明如何在 SuperMap iDesktop 上扩展开发自定义浮动面板 （如：工作空间管理器 WorkspaceControlManager、图层管理器 LayersControlManager 等浮动面板）。
